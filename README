asyncwr - An asynchronous writer package for Go


WHAT IS THIS?

  When writing Go code, there are times when I just want to write a message to
  some network socket or other destination, but don't particularly care about
  when that write is going to happen.

  This package implements an asynchronous implementation of the io.Writer
  interface. It can be used like any other writer, except that calls to Write()
  do not block and you do not need an explicit call to some blocking Flush().
  Anything that is written to the buffer is flushed automatically in the
  background.

  Additionally, this package allows multiple goroutines to append data to the
  buffer concurrently, and provides a notification channel to allow for
  separating the error handling from the writing code.


IMPORT PATH

  import "blicky.net/asyncwr"


USAGE

  import (
    "blicky.net/asyncwr"
    "fmt"
    "net"
  )

  wr := asyncwr.New(os.Stdout, 1024*1024)

  // Async error handling
  go func() {
    if e := <-wr.ErrorCh(); e != nil {
      fmt.Fprintf(os.Stderr, "Write error: %s.\n", e.Error())
      os.Exit(1)
    }
  }()

  wr.Write([]byte("Data\n"))

  // Do some other work

  <-wr.FlushCh()


MINI-FAQ:

  Q: Asynchronous behaviour!? This goes against the fundamental ideas of Go!

    Indeed. But I hate it when a programming language forces you into thinking
    that a single paradigm is enough for every situation you will ever
    encounter in your life. I prefer to have the freedom to do whatever I want
    to do, and luckily Go doesn't make that impossible.

  Q: What about an asynchronous reader?

    The idiomatic way to do asynchronous reads is to create a separate
    goroutine to (blocking) read from a connection and pass the results over a
    channel. I've not found any problems with that yet. :-)

  Q: What about performance?

    I haven't done any benchmarks, but I suppose this writer will be a bit
    slower than a bufio.Writer.

  Q: You're using locks instead of channels!

    I just happened to find a mutex a relatively easy synchronisation method
    for this purpose. It may also be a bit faster than an implementation based
    on channels, but I can't really comment on that without benchmarks.

  Q: Your usage example is bullshit!

    It's just an example. Suggestions for a better example are always welcome.


TODO

  - Tests!
  - Configuring and handling timeouts?
  - Call .Flush() on the underlying writer if it supports that?
